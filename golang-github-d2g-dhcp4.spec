%global goipath github.com/d2g/dhcp4
%global commit  a1d1b6c41b1ce8a71a5121a9cee31809c4707d9c
%gometa

Name:           golang-github-d2g-dhcp4
Version:        0
Release:        0.11
Summary:        DHCP library implemented in Go
License:        BSD
URL:            https://github.com/d2g/dhcp4
Source0:        https://github.com/d2g/dhcp4/archive/a1d1b6c41b1ce8a71a5121a9cee31809c4707d9c/dhcp4-a1d1b6c41b1ce8a71a5121a9cee31809c4707d9c.tar.gz
Source1:        glide.lock
Source2:        glide.yaml

BuildRequires:  compiler(go-compiler)

%description
DHCP4 library written in Go.

%package devel
Summary:       DHCP4 library written in Go
BuildArch:     noarch

%description devel
DHCP4 library written in Go.

This package contains the source code needed for building packages that
reference the following Go import paths: – github.com/d2g/dhcp4.

%prep
%forgesetup
cp %{SOURCE1} %{SOURCE2} .

%install
%goinstall glide.lock glide.yaml

%check
%gochecks

%files devel -f devel.file-list
%doc README.md LICENSE

%changelog
* Thu Dec 12 2019 wangzhishun <wangzhishun1@huawei.com> - 0-0.11
- Package init

